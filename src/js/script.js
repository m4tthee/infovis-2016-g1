var dataSets = {"bodyfat.csv" : {non_numeric: [], title: "Body Fat"}, "cereal.csv" : {non_numeric: ["Name","Manufacturer","Hot/Cold"], title: "Cereal"}, "cities2012.csv" : {non_numeric: ["Cities"], title: "Cities"}, "smallTest.csv" : {non_numeric: [], title: "Test"}};
setupBeforeDrawing();
loadDataset(Object.keys(dataSets)[0]);

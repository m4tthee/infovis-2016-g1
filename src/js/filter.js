var filterDefs = {}; // map of "Dim NAME" : [x,y]
var useAlphaForFilter = false;


function setupFilters() {
  var steps = 100.0;

  var filterTabList = d3.select("div.tab-content#filters ul");
  var filters = filterTabList.selectAll("li").data(validAxisDimensions);

  filters.exit().remove();

  var filterItem = filters.enter().append("li");
  filterItem.append("input").attr("class", "lower");
  filterItem.append("span");
  filterItem.append("input").attr("class", "upper");

  // lower bound input
  filters.select("input.lower")
    .attr("type", "number")
    .attr("id", function(d) { return d + "-lower"; })
    .attr("data-dim", function(d) { return d; })
    .attr("placeholder", "min")
    .attr("class", "lower")
    .attr("min", function(d) { return dimensions[d].extent[0]; })
    .attr("max", function(d) { return dimensions[d].extent[1]; })
    .attr("value", function(d) { return dimensions[d].extent[0]; })
    //.attr("step", function(d) { return (dimensions[d].extent[1] - dimensions[d].extent[0]) / steps; })
    .on("input", function() {
      var upperBound = document.getElementById(this.getAttribute("data-dim") + "-upper");
      var low = parseInt(this.value);
      var high = parseInt(upperBound.value);
      if (high < low) {
        upperBound.value = low;
		  onDimMaxFilterChanged(this.getAttribute("data-dim"), low);
      }
      onDimMinFilterChanged(this.getAttribute("data-dim"), low);
      return false;
    });
  // text
  filters.select("span")
    .text(function(d) {
      return d;
    })
    .attr("title", function(d) {
      return d;
    });
  // upper bound input
filters.select("input.upper")
    .attr("type", "number")
    .attr("id", function(d) { return d + "-upper"; })
    .attr("data-dim", function(d) { return d; })
    .attr("placeholder", "max")
    .attr("class", "upper")
    .attr("min", function(d) { return dimensions[d].extent[0]; })
    .attr("max", function(d) { return dimensions[d].extent[1]; })
    .attr("value", function(d) { return dimensions[d].extent[1]; })
    //.attr("step", function(d) { return (dimensions[d].extent[1] - dimensions[d].extent[0]) / steps; })
    .on("input", function() {
      var lowerBound = document.getElementById(this.getAttribute("data-dim") + "-lower");
      var high = parseInt(this.value);
      var low = parseInt(lowerBound.value);
      if (high < low) {
        lowerBound.value = high;
		  onDimMinFilterChanged(this.getAttribute("data-dim"), high);
      }
      onDimMaxFilterChanged(this.getAttribute("data-dim"), high);
      return false;
    });

  var resetButton = d3.select("div.tab-content#filters #reset-filters");
  resetButton.on("click", function() {
    var lowerBounds = filterTabList.selectAll("input.lower");
    lowerBounds[0].forEach(function(input) {
      input.value = input.getAttribute("min");
      onDimMinFilterChanged(input.getAttribute("data-dim"), input.value);
    });
    var upperBounds = filterTabList.selectAll("input.upper");
    upperBounds[0].forEach(function(input) {
      input.value = input.getAttribute("max");
      onDimMaxFilterChanged(input.getAttribute("data-dim"), input.value);
    });
  });

  var filterStyleToggle = d3.select("div.tab-content#filters #filter-style-select");
  filterStyleToggle.on("change", function() {
    onUseAlphaForFilterChanged(this.value === "fade");
  });

  var lowerBounds = filterTabList.selectAll("input.lower");
  lowerBounds[0].forEach(function(input) {
    input.value = input.getAttribute("min");
    onDimMinFilterChanged(input.getAttribute("data-dim"), input.value);
  });
  var upperBounds = filterTabList.selectAll("input.upper");
  upperBounds[0].forEach(function(input) {
    input.value = input.getAttribute("max");
    onDimMaxFilterChanged(input.getAttribute("data-dim"), input.value);
  });

  onUseAlphaForFilterChanged(d3.select("div.tab-content#filters #filter-style-select")[0].value === "fade");
}

function updateFilteredElements() {
  innerG.group.selectAll(".scatterPoints").each(function(d, i) {
    var elem = d3.select(this);
    if (useAlphaForFilter) {
      elem.attr("opacity", getVisibibilityOfDatum(d) ? 1 : 0.2);
      elem.attr("visibility", "visible");
    } else {
      elem.attr("visibility", getVisibibilityOfDatum(d) ? 'visible' : 'hidden');
      elem.attr("opacity", 1);
    }
  });
}

function onDimMinFilterChanged(dim, min) {
  filterDefs[dim][0] = min;
  updateFilteredElements();
}

function onDimMaxFilterChanged(dim, max) {
  filterDefs[dim][1] = max;
  updateFilteredElements();
}

function onUseAlphaForFilterChanged(newValue) {
  if (useAlphaForFilter != newValue) {
    useAlphaForFilter = newValue;
    innerG.group.selectAll(".scatterPoints").each(function(d, i) {
      var elem = d3.select(this);
      if (useAlphaForFilter) {
        elem.attr("opacity", (elem.attr("visibility") == "visible") ? 1 : 0.2);
        elem.attr("visibility", 'visible');
      } else {
        elem.attr("visibility", (parseFloat(elem.attr("opacity")) == 1) ? 'visible' : 'hidden');
        elem.attr("opacity", 1);
      }
    });
  }
}
var container = document.querySelectorAll("div.vis")[0];
var style = window.getComputedStyle ? getComputedStyle(container, null) : container.currentStyle;
var width = parseInt(d3.select("div.vis").style("width"))-convertRem(2);
var height = parseInt(d3.select("div.vis").style("height"));

var sectionsHoriz = {
  // percent
  yaxis: 0.15,
  draw: 0.80,
  rightMargin: 0.05,
}

var sectionsVert =  {
  // percent
  topMargin: 0.05,
  draw: 0.85,
  xaxis: 0.10
}

var svg = d3.select(container).append("svg")
  .attr("width", width)
  .attr("height", height)
  .attr("id", "graph");
  //.attr("viewBox", "0 0 " + width + " " + height);
d3.select(window).on('resize', resize);

var YDimensionsG = {
  updateTransformation: function() { this.group.attr("transform", "translate(" + width * sectionsHoriz.yaxis + "," + height * sectionsVert.topMargin + ")")},
  group: svg.append("g"),
  innerH: function() { return height * sectionsVert.draw; },
  innerW: function() { return width * sectionsHoriz.yaxis; }
};

var XDimensionsG = {
  updateTransformation: function() { this.group.attr("transform", "translate(" + width * sectionsHoriz.yaxis + "," + height * (sectionsVert.draw + sectionsVert.topMargin)+ ")")},
  group: svg.append("g"),
  innerH: function() { return height * sectionsVert.xaxis; },
  innerW: function() { return width * sectionsHoriz.draw; }
};

var innerG = {
  updateTransformation: function() { this.group.attr("transform", "translate(" + width * sectionsHoriz.yaxis + "," + height * sectionsVert.topMargin + ")")}, //scale(" + this.innerW() + "," + this.innerH() +")"); },
  group: svg.append("g").attr("class", "innerG"),
  innerH: function() { return height * sectionsVert.draw; },
  innerW: function() { return width * sectionsHoriz.draw; },
};

var xScale = d3.scale.linear().range(["0%", sectionsHoriz.draw * 100 + "%"]);
var yScale = d3.scale.linear().range([sectionsVert.draw * 100 + "%", "0%"]);

var xAxisScale;
var yAxisScale;

var xAxis = {
  axis: d3.svg.axis().scale(d3.scale.linear()).orient("bottom").tickFormat(axisLabelFormatter),
  updateAxisRange: function () { this.axis.scale().range([0, XDimensionsG.innerW()]);}
}

var yAxis = {
  axis: d3.svg.axis().scale(d3.scale.linear()).orient("left").tickFormat(axisLabelFormatter),
  updateAxisRange: function () { this.axis.scale().range([YDimensionsG.innerH(), 0]);}
}

var defaultValues = {
  radius: "0.5em",
  color: "tomato"
}

var dimensions = {}; // map of "NAME" : { extent: [MIN, MAX], non_numeric: BOOL}. if non_numeric == FALSE, it cannot be an axis and extent is invalid.
var validAxisDimensions;

function setupBeforeDrawing() {
  XDimensionsG.updateTransformation();
  YDimensionsG.updateTransformation();
  innerG.updateTransformation();
  xAxis.updateAxisRange();
  yAxis.updateAxisRange();

  svg.call(dimensionChooseTip);
  svg.call(tooltip);
  svg.call(zoomTooltip);
  svg.append("defs").append("clipPath").attr("id", "clipPenaltyCircle").append("rect").attr("x", 0 + "%").attr("y", 0 + "%").attr("width", sectionsHoriz.draw * 100 + "%").attr("height", sectionsVert.draw * 100 + "%");

  innerG.group.append("rect").attr("x", 0 + "%").attr("y", 0 + "%").attr("width", sectionsHoriz.draw * 100 + "%").attr("height", sectionsVert.draw * 100 + "%").attr("opacity", 0);

  setupDataSetSwitch();
  setupDistortion();
  setupGridSwitch();
  setupClusteringSwitch();
  setupGlyphsBeforeDrawing();
}

function setupAfterDrawing() {
  setupClusteringAfterDrawing();
  setupDimensionDrag();
  setupFilters();
  setupGlyphsAfterDrawing();
  innerG.group.call(zoom);
  updateVisibleElements();
  updateRadiusGlyph('disabled');
  updateColorGlyph('disabled');
}

function render(data) {
  xScale.domain(dimensions[currentX].extent);
  yScale.domain(dimensions[currentY].extent);

  xAxis.axis.scale().domain(dimensions[currentX].extent);
  yAxis.axis.scale().domain(dimensions[currentY].extent);
  zoom.x(xAxis.axis.scale())
      .y(yAxis.axis.scale())

  YDimensionsG.group.attr("class", "axis");
  XDimensionsG.group.attr("class", "axis");

  XDimensionsG.group.call(xAxis.axis);
  YDimensionsG.group.call(yAxis.axis);

  //second axis call is neccessary!
  checkAxesSize();
  XDimensionsG.group.call(xAxis.axis);
  YDimensionsG.group.call(xAxis.axis);

  innerG.group.selectAll(".scatterPoints").remove();

  var scatters = innerG.group.selectAll(".scatterPoints").data(data);
  scatters.enter().append("circle")
    .attr("class", "scatterPoints")
    .attr("cx", function(d) { return xScale(d[currentX]);})
    .attr("cy", function(d) { return yScale(d[currentY]);})
    .attr("r", defaultValues.radius)
    .attr("fill", defaultValues.color)
    .attr("visibility", 'visible')
    .attr("opacity", 1)
    .attr("clip-path", "url(#clipPenaltyCircle)")


  drawGrid();

  XDimensionsG.group.call(xAxis.axis);
  YDimensionsG.group.call(yAxis.axis);

  setupAfterDrawing();
}

function updateScatter() {
  innerG.group.selectAll(".scatterPoints")
  .attr("cx", function(d) { return xScale(d[currentX]);})
  .attr("cy", function(d) { return yScale(d[currentY]);})
  .attr("stroke", "none").attr("stroke-width", "0.0em");
  updateVisibleElements();
}

function resize() {
  width = parseInt(d3.select("div.vis").style("width"))-convertRem(2);
  height = parseInt(d3.select("div.vis").style("height"));

  svg.attr("width", width)
    .attr("height", height);

  YDimensionsG.updateTransformation();
  XDimensionsG.updateTransformation();
  innerG.updateTransformation();

  xAxis.updateAxisRange();
  XDimensionsG.group.call(xAxis.axis);
  yAxis.updateAxisRange();
  YDimensionsG.group.call(yAxis.axis);

  //we need to call the xaxis twice in order to check the overlap of the resized elements!!
  checkAxesSize();
  XDimensionsG.group.call(xAxis.axis);
  YDimensionsG.group.call(yAxis.axis);

  //resize fisheye lens
  fisheyeCircle
      .attr("rx", fisheyeRadius + "%")
      .attr("ry", fisheyeRadius * innerG.innerW() /innerG.innerH() + "%")     //recalculate semi-minor axis to yield circular shape

  //update grid.
  drawGrid();
}

function loadDataset(name) {
  updateDatasetSwitch(name);
  d3.csv("datasets/" + name,
  function(datum) {
    Object.keys(datum).forEach(function(key) {
      if (dataSets[name].non_numeric.indexOf(key) < 0) {
        datum[key] = +datum[key];
      }
    });

    return datum;
  },
  function(data) {
    dimensions = {};
    filterDefs = {};
    var setCurrent = false;
    if (d3.keys(data[0]).indexOf("RecordId") == -1) {
      data = data.map(function(d, i) {
        var tmpD = {RecordId: i};
        d["RecordId"] = i;
        d3.keys(data[0]).forEach(function(c) {
          tmpD[c] = d[c];
        });
        return tmpD;
      });
    }

    Object.keys(data[0]).forEach(function(d) {
      var dimensionObj = {isValidAxisDimension: (dataSets[name].non_numeric.indexOf(d) < 0)};
      if (dimensionObj.isValidAxisDimension) {
        if (!setCurrent) {
          currentX = d;
          currentY = d;
          setCurrent = true;
        }

        var extent = d3.extent(data.map(function(datum){
          return datum[d];
        }));

        //increase extent by 5% for a better viewing experience
        //dimensionObj["extent"] = [extent[0] - (Math.abs(extent[0]) * 0.05), extent[1] + (Math.abs(extent[1]) * 0.05)]

        dimensionObj["extent"] = extent;
        filterDefs[d] = [extent[0], extent[1]];

      } else {
        dimensionObj["extent"] = [0,0];
      }
      dimensions[d] = dimensionObj;
    });

    validAxisDimensions = Object.keys(data[0]).filter(function(d) {
      return dimensions[d].isValidAxisDimension;
    });
    shownDimension = Object.keys(data[0])[0];
    glyphs.color.startingDimension = validAxisDimensions[0];
    glyphs.radius.onDimensionChanged(validAxisDimensions[0]);
    glyphs.color.onDimensionChanged(validAxisDimensions[0]);
    render(data);
  });
}

function axisLabelFormatter(value) {
  if (Math.abs(value) >= 10) {
    var prefix = d3.formatPrefix(value, 3);
    var valueAsString = d3.round(prefix.scale(value).toFixed(4), 3).toString();
    return valueAsString + prefix.symbol;
  } else {
    return d3.round(value.toFixed(7), 6);
  }
}

function checkAxesSize() {
  //check height / width
  var scale = 1;
  scale = Math.min(scale, YDimensionsG.innerW() / YDimensionsG.group.node().getBBox().width);
  scale = Math.min(scale, XDimensionsG.innerH() / XDimensionsG.group.node().getBBox().height);

  if (scale < 1) {
    XDimensionsG.group.selectAll(".tick text").attr("font-size", scale + "em");
    YDimensionsG.group.selectAll(".tick text").attr("font-size", scale + "em");
  } else {
    XDimensionsG.group.selectAll(".tick text").attr("font-size", "1em");
    YDimensionsG.group.selectAll(".tick text").attr("font-size", "1em");
  }

  //check xaxis label overlap
  var ticks = XDimensionsG.group.selectAll(".tick text");
  var maxWidth = 0;

  ticks.each(function(d, i) {
    maxWidth = Math.max(maxWidth, d3.select(this).node().getBBox().width);
  });

  xAxis.axis.ticks(Math.max(Math.floor(innerG.innerW() * 0.5 / maxWidth )), 2);

  //check yaxis label overlap
  ticks = YDimensionsG.group.selectAll(".tick text");
  var maxHeight = 0;

  ticks.each(function(d, i) {
    maxHeight = Math.max(maxHeight, d3.select(this).node().getBBox().height);
  });

  yAxis.axis.ticks(Math.max(Math.floor(innerG.innerH() * 0.5 / maxHeight )), 2);
}

function getVisibibilityOfDatum(d) {
  var visibleFlag = true;
  validAxisDimensions.forEach(function(dim) {
    if (d[dim] < filterDefs[dim][0] || d[dim] > filterDefs[dim][1]) {
        visibleFlag = false;
    }
  });
  return visibleFlag;
}

//HTML event handling
function updateDatasetSwitch(name) {
  d3.selectAll("#dataset-switch li.active").attr("class", "")
  var link = d3.select('#dataset-switch a[data-dataset|="' + name + '"]');

  var li = link[0][0].parentElement;
  d3.select(li).attr("class", "active");
}

function setupDataSetSwitch() {
  datasetArray = [];

  Object.keys(dataSets).forEach(function(k){
    datasetArray.push({
      dataset: k,
      title: dataSets[k].title
    })
  })
  var switchContainer = d3.select("#dataset-switch")
  var dataset = switchContainer.selectAll("li").data(datasetArray);

  dataset.enter()
    .append("li")
    .append("a")
    .attr("href", "#")
    .on("click", function(d){
      d3.event.stopPropagation()
      d3.event.preventDefault()

      var link = d3.select(this);

      loadDataset(link.attr("data-dataset"));
    })

  dataset.exit().remove();
  dataset.select("a")
    .text(function(d){
      return d.title;
    })
    .attr("data-dataset", function(d){
      return d.dataset;
    });
}



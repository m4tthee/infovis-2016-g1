//---------------------------------------------------------------------------------------------------------------
//gridlines
var gridEnabled = true;

var gridSubdiv = 10;
var gridLine = d3.svg.line();
var gridPath;

var xScaleGrid = d3.scale.linear().range([0, innerG.innerW()]);
var yScaleGrid = d3.scale.linear().range([innerG.innerH(), 0]);

function drawGrid() {
    //delete all data paths
    innerG.group.selectAll(".x").remove();
    innerG.group.selectAll(".y").remove();
    innerG.group.selectAll("path").remove();

    if(!gridEnabled)
        return;

    //create array containing tick positions
    var xSteps = xAxis.axis.scale().ticks(xAxis.axis.ticks()[0]);
    var ySteps = yAxis.axis.scale().ticks(yAxis.axis.ticks()[0]);

    //add lowest / highest value on axis to tick positions array
    xSteps.unshift(d3.min(xAxis.axis.scale().domain()));
    xSteps.push(d3.max(xAxis.axis.scale().domain()));
    ySteps.unshift(d3.min(yAxis.axis.scale().domain()));
    ySteps.push(d3.max(yAxis.axis.scale().domain()));

    //subdivide tick position arrays
    var xStepsFine = d3.range(d3.min(xSteps), d3.max(xSteps), (d3.max(xSteps) - d3.min(xSteps)) / ( xSteps.length * gridSubdiv));
    var yStepsFine = d3.range(d3.min(ySteps), d3.max(ySteps), (d3.max(ySteps) - d3.min(ySteps)) / ( ySteps.length * gridSubdiv));

    //use undistorted scale for initial grid.
    xScaleGrid.domain(xAxis.axis.scale().domain()).range([0, innerG.innerW()]);
    yScaleGrid.domain(yAxis.axis.scale().domain()).range([innerG.innerH(), 0]);

    //lines in x direction
    innerG.group.selectAll(".x")
        .data(xSteps)
        .enter().append("path")
        .attr("class", "x")
        .datum(function(x)  {
            return yStepsFine.map(function(y) {
                return [xScaleGrid(x),
                        yScaleGrid(y)];
                });
        });

    //lines in y direction
    innerG.group.selectAll(".y")
        .data(ySteps)
        .enter().append("path")
        .attr("class", "y")
        .datum(function(y)  {
            return xStepsFine.map(function(x) {
                return [xScaleGrid(x),
                        yScaleGrid(y)];
                });
        });

    //add paths to SVG
    gridPath = innerG.group.selectAll("path")
        .attr("d", gridLine)
        .attr("stroke","#B0B0B0")
        .attr("stroke-width","0.05em")
        .attr("fill-opacity", 0);

    //move circoes in front of the grid.
    innerG.group.selectAll(".scatterPoints").moveToFront();
}

function updateGrid(value){
  switch (value) {
  case "enabled":
    gridEnabled = true;
    break;
  case "disabled":
    gridEnabled = false;
  }

  d3.selectAll("#grid-switch li.active").attr("class", "");

  var li = d3.select("#grid-switch a[data-grid|=" + value + "]")[0][0].parentElement;
  d3.select(li).attr("class", "active");

  drawGrid();
  
  //distort grid immediately if distortion is enabled.
  updateDistortion(innerG.group[0][0]);
    
}

function setupGridSwitch() {
  d3.selectAll("#grid-switch a")
    .on("click", function(){
      var link = d3.select(this);

      updateGrid(link.attr("data-grid"));

      d3.event.stopPropagation();
      d3.event.preventDefault();
    });
  updateGrid("disabled");
}
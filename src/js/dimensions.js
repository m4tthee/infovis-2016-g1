var currentDragDimension = "";
var currentX = "";
var currentY = "";

var dimensionChooseTip = d3.tip()
  .attr('class', 'd3-tip')
  .offset([0, 0])
  .html(function() {return currentDragDimension;});


function setupDimensionDrag() {
	var XDimensionDrag = d3.select("div.dim-select.horizontal");
  XDimensionDrag.selectAll("ul").remove();
	var XDimensionList = XDimensionDrag.append("ul");

	var YDimensionDrag = d3.select("div.dim-select.vertical");
  YDimensionDrag.selectAll("ul").remove();
	var YDimensionList = YDimensionDrag.append("ul");

	XDimensionList.selectAll("li").data(validAxisDimensions).enter()
		.append("li").append("span").text(function(d) {
			return d;
		});

	YDimensionList.selectAll("li").data(validAxisDimensions).enter()
		.append("li").append("span").text(function(d) {
			return d;
		});


	var initialDragX = 0;
	var marginX = parseFloat(XDimensionList.style("margin-left"));
	var initialPixelMarginX = marginX;
	var initialPercentageX = 30;
	var stepSizeX = -40;
	var maxMarginX = initialPercentageX + (validAxisDimensions.length - 1) * stepSizeX;
	XDimensionDrag.call(d3.behavior.drag()
		.on("dragstart", function() {
			initialDragX = d3.mouse(this)[0];
		})
		.on("drag", function() {
			XDimensionList.style("margin-left", appendUnit(marginX + (d3.mouse(this)[0] - initialDragX)));
		})
		.on("dragend", function() {
			marginX += d3.mouse(this)[0] - initialDragX;
			var snappingInfo = getNearestSnappingPercentage(marginX, initialPercentageX, stepSizeX, initialPixelMarginX, maxMarginX)
			XDimensionList.style("margin-left", appendUnit(snappingInfo[0], "%"));
			marginX = parseFloat(XDimensionList.style("margin-left"));
			currentX = validAxisDimensions[snappingInfo[1]];
      xDimensionChanged();
    })
	);

	var initialDragY = 0;
	var marginY = parseFloat(YDimensionList.style("margin-left"));
	var initialPixelMarginY = marginY;
	var initialPercentageY = 30;
	var stepSizeY = -40;
	var maxMarginY = initialPercentageY + (validAxisDimensions.length - 1) * stepSizeY;
	YDimensionDrag.call(d3.behavior.drag()
		.on("dragstart", function() {
			initialDragY = d3.mouse(this)[1];
		})
		.on("drag", function() {
			YDimensionList.style("margin-left", appendUnit(marginY - (d3.mouse(this)[1] - initialDragY)));
		})
		.on("dragend", function() {
			marginY -= d3.mouse(this)[1] - initialDragY;
			var snappingInfo = getNearestSnappingPercentage(marginY, initialPercentageY, stepSizeY, initialPixelMarginY, maxMarginY)
			YDimensionList.style("margin-left", appendUnit(snappingInfo[0], "%"));
			marginY = parseFloat(YDimensionList.style("margin-left"));
			currentY = validAxisDimensions[snappingInfo[1]];
      yDimensionChanged();
		})
	);
}

function xDimensionChanged() {
  xScale.domain(dimensions[currentX].extent);
  xAxis.axis.scale().domain(dimensions[currentX].extent);
  XDimensionsG.group.call(xAxis.axis);
  innerG.group.selectAll("circle").attr("cx", function(d) {
    return xScale(d[currentX]);
  });
  zoom.x(xAxis.axis.scale());
  ///resetView();

  //neccessary
  checkAxesSize();
  XDimensionsG.group.call(xAxis.axis);
  YDimensionsG.group.call(yAxis.axis);

    xScalePercent.domain(xAxis.axis.scale().domain())
    drawGrid();
    zoomed();
}

function yDimensionChanged() {
      yScale.domain(dimensions[currentY].extent);
      yAxis.axis.scale().domain(dimensions[currentY].extent);
      YDimensionsG.group.call(yAxis.axis);
      innerG.group.selectAll("circle").attr("cy", function(d) {
				return yScale(d[currentY]);
			});
      zoom.y(yAxis.axis.scale());
      //resetView();

  //neccessary
  checkAxesSize();
  XDimensionsG.group.call(xAxis.axis);
  YDimensionsG.group.call(yAxis.axis);

    yScalePercent.domain(yAxis.axis.scale().domain())
    drawGrid();
    zoomed();
}



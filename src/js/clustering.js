var clusteringEnabled = false;
var isCurrentlyClustering = false;

var tooltip = d3.tip()
  .attr('class', 'd3-tip2')
  .offset([0, 0])
  .html(function(d, i) {
    var output = '<span class="tooltip">';
    findClusterElements(d).each(function(datum) {
      output += datum[shownDimension] + "<br>"
    });
    return output + "</span>";
  });
  
function setupClusteringAfterDrawing() {
  innerG.group.selectAll(".scatterPoints")
    .on('mouseenter', function(d) {
      d3.event.stopImmediatePropagation();

      if (!isCurrentlyClustering) {
        tooltip.show(d);
      }
    })
  .on('mouseout', function() {
    d3.event.stopImmediatePropagation();

    tooltip.hide();
    if (isCurrentlyClustering) {
      innerG.group.selectAll(".clusterText").remove();
      updateScatter();
      isCurrentlyClustering = false;
    }
  })
  .on('mousedown', startCluster)
  .on('touchstart', startCluster)
  .on("mouseup", endCluster);

  svg.on('touchend', endCluster);
}
  
function startCluster() {
  if (clusteringEnabled) {
    d3.event.stopImmediatePropagation();
    tooltip.hide();

    isCurrentlyClustering = true;
    var elem = d3.select(this)
      .attr("stroke", "steelblue")
      .attr("stroke-width", "0.1em");
    innerG.group.append("text")
      .attr("class", "clusterText")
      .text(elem.datum()[shownDimension])
      .attr("x", elem.attr("cx"))
      .attr("y", elem.attr("cy"))
      .attr("font-size", "0.8em")
      .attr("text-anchor", "middle")
      .attr("pointer-events", "none")
      .attr("dy", "0.4em");

    var clusters = findClusterElements(elem.datum());
    if (clusters.size() == 1) {
      return;
    }

    var angle = 1 / (clusters.size() - 1);
    var index = 0;
    clusters.each(function(d, i) {
      var tCircle = d3.select(this);

      if (d != elem.datum()) {
        var size = 2 + 18 * Math.min(clusters.size(), 20) / 20;
        tCircle.attr("cx", (percentStringToFloat(tCircle.attr("cx")) + Math.cos(2 * Math.PI * index * angle + Math.PI/2) * size  * height / width) + "%")
               .attr("cy", (percentStringToFloat(tCircle.attr("cy")) + Math.sin(2 * Math.PI * index * angle + Math.PI/2) * size) + "%")
               .attr("stroke", "steelblue")
               .attr("stroke-width", "0.1em");
        innerG.group.append("text")
          .attr("class", "clusterText")
          .text(d[shownDimension])
          .attr("x", tCircle.attr("cx"))
          .attr("y", tCircle.attr("cy"))
          .attr("font-size", "0.8em")
          .attr("text-anchor", "middle")
          .attr("pointer-events", "none")
          .attr("dy", "0.4em");
        ++index;
      }
    });
  }
}

function endCluster() {
  if (clusteringEnabled && isCurrentlyClustering) {
    innerG.group.selectAll(".clusterText").remove();
    updateScatter();
    isCurrentlyClustering = false;
  }
}

function findClusterElements(d) {
  return innerG.group.selectAll(".scatterPoints").filter(function(datum) {
    return d[currentX] == datum[currentX] && d[currentY] == datum[currentY];
  });
}

//HTML event handling
function updateClustering(value){
  switch (value) {
  case "enabled":
    clusteringEnabled = true;
    break;
  case "disabled":
    clusteringEnabled = false;
    break;
  }

  d3.selectAll("#clustering-switch li.active").attr("class", "");

  var li = d3.select("#clustering-switch a[data-clustering|=" + value + "]")[0][0].parentElement;
  d3.select(li).attr("class", "active");
}

function setupClusteringSwitch() {
  d3.selectAll("#clustering-switch a")
    .on("click", function(){
      var link = d3.select(this);

      updateClustering(link.attr("data-clustering"));

      d3.event.stopPropagation();
      d3.event.preventDefault();
    });
  updateClustering("disabled");
}


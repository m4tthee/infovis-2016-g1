var visibleElements = [];

function updateVisibleElements() {
  var clipPath = svg.select("#clipPenaltyCircle > rect");
  var tmpVisibleElements = [];

  var scatters = innerG.group.selectAll(".scatterPoints").each( function(d, i){
    var d3Elem = d3.select(this);
    var cx = percentStringToFloat(d3Elem.attr("cx"));
    var cy = percentStringToFloat(d3Elem.attr("cy"));

    if (d3Elem.attr("visibility") == "visible" &&
        cx >= percentStringToFloat(clipPath.attr("x")) && cx <= percentStringToFloat(clipPath.attr("width")) &&
        cy >= percentStringToFloat(clipPath.attr("y")) && cy <= percentStringToFloat(clipPath.attr("height"))) {
      tmpVisibleElements.push(d);
    }
  });

  if (!arraysEqual(visibleElements, tmpVisibleElements)) {
    visibleElements = tmpVisibleElements;
    visibleElementsChangedEvent(tmpVisibleElements);
  }
}

function onListElementMouseOver(d) {
  innerG.group.selectAll(".scatterPoints").filter(function(datum) {
    return datum == d;
  }).attr("stroke", "black").attr("stroke-width", "0.1em");
}

function onListElementMouseOut(d) {
  innerG.group.selectAll(".scatterPoints").filter(function(datum) {
    return datum == d;
  }).attr("stroke", "none").attr("stroke-width", "0.0em");
}

var visibleElementsChanged = function(elements) {
  container = d3.select("#list-of-elements ul.visible-elements")
  var listItem = container.selectAll("li.element").data(elements);

  listItem.enter()
    .append("li")
    .attr("class", "element")
    .on("mouseover", onListElementMouseOver)
    .on("mouseout", onListElementMouseOut);

  listItem.exit().remove();
  listItem
    .html(function(d){
      var text = []
      Object.keys(d).forEach(function(k){
        if (typeof(d[k]) === "object") {
          return;
        }

        text.push("<strong>" + k + ":</strong> " + d[k]);
      });

      return text.join("<br>");
    });
  container[0][0].scrollTop = 0;

  if (elements.length == 0) {
    container.append("li").attr("class", "no-elements").html("No visible elements.");
  } else {
    container.select("li.no-elements").remove()
  }
}.throttle(1000);

function visibleElementsChangedEvent(elements) {
  visibleElementsChanged(elements);
}
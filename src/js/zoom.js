var zoom = d3.behavior.zoom()
  .scaleExtent([0.0001, 99])
  .on("zoom", zoomed);

 var zoomTooltip = d3.tip()
.attr('class', 'd3-tip3')
.offset([0, 0])
.html(function(d, i) {return "Click to reset view";});
  
var zoomInfo = innerG.group.append("text").attr("x", sectionsHoriz.draw * 100 + "%").attr("y", -sectionsVert.topMargin * 100 + "%").attr("dy", "1em").attr("text-anchor", "end").text(toPercent(zoom.scale()))
  .on('mouseenter', zoomTooltip.show)
  .on('mouseout', zoomTooltip.hide)
  .on('click', resetView);


function setupZoom() {
  zoom = d3.behavior.zoom()
    .scaleExtent([0.0001, 99])
    .on("zoom", zoomed);
}

function enableZoom()
{
  innerG.group.call(zoom);
}

function disableZoom() {
  innerG.group.on('.zoom', null);
}

function resetView() {
  zoom.scale(1);
  zoom.translate([0,0]);
  zoomed();
}

function zoomed() {
  XDimensionsG.group.call(xAxis.axis);
  YDimensionsG.group.call(yAxis.axis);
  xScale.domain(xAxis.axis.scale().domain());
  yScale.domain(yAxis.axis.scale().domain());
  xScalePercent.domain(xAxis.axis.scale().domain());
  yScalePercent.domain(yAxis.axis.scale().domain());
  xScaleGrid.domain(xAxis.axis.scale().domain());
  yScaleGrid.domain(yAxis.axis.scale().domain());
  drawGrid();
  updateScatter();
  zoomInfo.text(toPercent(zoom.scale()));

  updateDistortion(svg.select(".innerG").node());
}
/// from: http://bl.ocks.org/mbostock/7555321
function wrap(text, width) {
  text.each(function() {
    var text = d3.select(this),
        words = text.text().split(/\s+/).reverse(),
        word,
        line = [],
        lineNumber = 0,
        lineHeight = 1.1, // ems
        y = text.attr("y"),
        dy = parseFloat(text.attr("dy")),
        tspan = text.text(null).append("tspan").attr("x", 0).attr("y", y).attr("dy", dy + "em");
    while (word = words.pop()) {
      line.push(word);
      tspan.text(line.join(" "));
      if (tspan.node().getComputedTextLength() > width) {
        line.pop();
        tspan.text(line.join(" "));
        line = [word];
        tspan = text.append("tspan").attr("x", 0).attr("y", y).attr("dy", ++lineNumber * lineHeight + dy + "em").text(word);
      }
    }
  });
}

function toPercent(value) {
  return Math.round(1000 * value) / 10 + "%";
}

function getNearestSnappingPercentage(value, start, step, referencePixels, maxMargin) {
	var onePercent = referencePixels / start;
	var p = start;
	var i = 0;
	while (p > maxMargin) {
		var currentPercent = value / onePercent;
		if (p - currentPercent < (-step / 2)) {
			return [p, i];
		} else if(currentPercent > start) {
			return [start, 0];
		}
		p += step;
		i++;
	}

	return [maxMargin, i];
}

function appendUnit(value, unit) {
  if (unit === undefined) {
    unit = "px";
  }

  return value.toString() + unit;
}

function percentStringToFloat(string) {
  return parseFloat(string.replace("%", ""));
}

/// from: http://stackoverflow.com/questions/3115982/how-to-check-if-two-arrays-are-equal-with-javascript/16436975#16436975
function arraysEqual(a, b) {
  if (a === b) return true;
  if (a == null || b == null) return false;
  if (a.length != b.length) return false;

  for (var i = 0; i < a.length; ++i) {
    if (a[i] !== b[i]) return false;
  }
  return true;
}

function getRootElementFontSize( ) {
    // Returns a number
    return parseFloat(
        // of the computed font-size, so in px
        getComputedStyle(
            // for the root <html> element
            document.documentElement
        )
        .fontSize
    );
}

function convertRem(value) {
    return value * getRootElementFontSize();
}

//removes the object from the DOM tree and readds it as the last child.
d3.selection.prototype.moveToFront = function() {
  return this.each(function(){
    this.parentNode.appendChild(this);
  });
};

var shownDimension = "";

var glyphs = {
  color: {
    enabled: false,
    startingDimension: "",
    scale: d3.scale.linear().range([colorbrewer.YlGn[3][0], colorbrewer.YlGn[3][2]]),
    onDimensionChanged: function(dimensionName) {
        console.log("starting dim:" , dimensionName);
        if (dimensionName === undefined) {
          dimensionName = glyphs.color.startingDimension;
        }
        if (validAxisDimensions.indexOf(dimensionName) == -1) {
          return;
        }
        this.scale.domain(dimensions[dimensionName].extent);
        innerG.group.selectAll(".scatterPoints").attr("fill", function(d) {
          return glyphs.color.scale(d[dimensionName]);
          });
      },
    onDisable: function() {
        innerG.group.selectAll(".scatterPoints").attr("fill", defaultValues.color);
      }
  },
  radius: {
    enabled: false,
    scale: d3.scale.linear().range([0.3, 1.0]),
    currentDimension: "",
    onRangeChanged: function (rangeExtent) {
        this.scale.range(rangeExtent);
        this.updateScatter();
      },
    onDimensionChanged: function(dimensionName) {
        if (validAxisDimensions.indexOf(dimensionName) == -1) {
          return;
        }
        this.currentDimension = dimensionName;
        this.scale.domain(dimensions[dimensionName].extent);
        this.updateScatter();
      },
    onDisable: function() {
        innerG.group.selectAll(".scatterPoints").attr("r", defaultValues.radius);
        glyphs.radius.enabled = false;
      },
    onEnable: function() {
      glyphs.radius.enabled = true;
      this.updateScatter();
    },
    updateScatter: function() {
      if (! glyphs.radius.enabled) {
        return;
      }
        innerG.group.selectAll(".scatterPoints").attr("r", function(d) {
          return glyphs.radius.scale(d[glyphs.radius.currentDimension]) + "em";
          });
      }
  }
};

function setupGlyphsBeforeDrawing() {
  setupRadiusSwitch();
  setupColorSwitch();
}

function setupGlyphsAfterDrawing() {
  setupDimensionSelects();
}

//HTML event handling
function setupDimensionSelects() {
  var selectDim = d3.select("select#tooltip-dim-select");
  selectDim.on("change", function() {
    shownDimension = this.value;
  });

  selectDim.selectAll("option").remove();
  var selectDimData = selectDim.selectAll("option").data(d3.keys(dimensions));

  selectDimData.enter().append("option")
    .attr("value", function(d) {
      return d;
    })
    .text(function(d) {
      return d;
    });

  selectDim = d3.select("select#color-dim-select");
  selectDim.on("change", function() {
      if (glyphs.color.enabled)
        glyphs.color.onDimensionChanged(this.value);
      else
        glyphs.color.startingDimension = this.value;
  });

  selectDim.selectAll("option").remove();
  var selectDimData = selectDim.selectAll("option").data(validAxisDimensions);

  selectDimData.enter().append("option")
    .attr("value", function(d) {
      return d;
    })
    .text(function(d) {
      return d;
    });

  selectDim = d3.select("select#radius-dim-select");
  selectDim.on("change", function() {
    glyphs.radius.onDimensionChanged(this.value);
  });

  selectDim.selectAll("option").remove();
  var selectDimData = selectDim.selectAll("option").data(validAxisDimensions);

  selectDimData.enter().append("option")
    .attr("value", function(d) {
      return d;
    })
    .text(function(d) {
      return d;
    });
}

function setupRadiusSwitch() {
  d3.selectAll("#glyph-radius-switch a")
    .on("click", function(){
      var link = d3.select(this);

      updateRadiusGlyph(link.attr("data-glyph-radius"));

      d3.event.stopPropagation();
      d3.event.preventDefault();
    });
  updateRadiusGlyph("disabled");

  var min = d3.select("input#radius-glyph-min");
  min[0][0].value = parseFloat(defaultValues.radius)
  var max = d3.select("input#radius-glyph-max")
  max[0][0].value = parseFloat(defaultValues.radius)

  min.on("input", function() {
    glyphs.radius.onRangeChanged([min[0][0].value, max[0][0].value]);
  });
  max.on("input", function() {
    glyphs.radius.onRangeChanged([min[0][0].value, max[0][0].value]);
  });
}

function setupColorSwitch() {
  d3.selectAll("#glyph-color-switch a")
    .on("click", function(){
      var link = d3.select(this);

      updateColorGlyph(link.attr("data-glyph-color"));

      d3.event.stopPropagation();
      d3.event.preventDefault();
    });
  updateColorGlyph("disabled");
}

function updateRadiusGlyph(value){
  console.log("Radius Glyph: " + value);
  switch (value) {
  case "enabled":
    var min = d3.select("input#radius-glyph-min")[0][0].value;
    var max = d3.select("input#radius-glyph-max")[0][0].value;
    glyphs.radius.onRangeChanged([min, max]);
    glyphs.radius.onEnable();
    break;
  case "disabled":
    glyphs.radius.onDisable();
  }

  d3.selectAll("#glyph-radius-switch li.active").attr("class", "");

  var li = d3.select("#glyph-radius-switch a[data-glyph-radius|=" + value + "]")[0][0].parentElement;
  d3.select(li).attr("class", "active");
}

function updateColorGlyph(value){
  console.log("Color glyph: " + value);
    glyphs.color.enabled = (value == "enabled");
  switch (value) {
  case "enabled":
    var selectDimColor = d3.select("select#color-dim-select")[0][0].value;
    glyphs.color.onDimensionChanged(selectDimColor);
    break;
  case "disabled":
    glyphs.color.onDisable();
  }

  d3.selectAll("#glyph-color-switch li.active").attr("class", "");

  var li = d3.select("#glyph-color-switch a[data-glyph-color|=" + value + "]")[0][0].parentElement;
  d3.select(li).attr("class", "active");
}


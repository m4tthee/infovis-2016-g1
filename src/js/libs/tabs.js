(function(){
  var enableTab = function(tab_id){
    console.log("Enabling tab #" + tab_id);
    
    d3.select(".tabs .tab-content.active").attr("class", "tab-content")
    d3.select(".tabs .tab-content#" + tab_id).attr("class", "tab-content active")
  }
  
  d3.selectAll(".tabs ul.bar li")
    .on("click", function(){
      d3.event.stopPropagation();
      d3.event.preventDefault();
      
      var li = d3.select(this)
      var tab_id = li.select("a").attr("data-tab")
      
      enableTab(tab_id)
      d3.selectAll(".tabs ul.bar li.active").attr("class", "")
      li.attr("class", "active")
      
  });
})();
(function(window, hljs){
  function attach_source(element, source_url) {
    var xhr = window.XMLHttpRequest ? new XMLHttpRequest() : new ActiveXObject("Microsoft.XMLHTTP");
    xhr.open('GET', source_url);
    xhr.onreadystatechange = function() {
        if (xhr.readyState>3 && xhr.status==200) {
          element.innerText = xhr.responseText
          hljs.highlightBlock(element);
        }
    };
    xhr.setRequestHeader('X-Requested-With', 'XMLHttpRequest');
    xhr.send(null);
  }

  window.addEventListener("load", function load(){
    window.removeEventListener("load", load, false);

    var elements = document.querySelectorAll("code[data-src]");

    for (var i = 0; i < elements.length; i++) {
      var element = elements[i],
        attr = element.attributes.getNamedItem("data-src");

      if (attr) {
        attach_source(element, attr.value)
      }
    }
  });
})(window, hljs);

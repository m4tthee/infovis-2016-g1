//---------------------------------------------------------------------------------------------------------------
//fisheye and cartesian distortion stuff
var distortionType = "none";

var fisheyeRadius = 5;
var fisheyeStrength = 4;

var fisheye = d3.fisheye.circular()
    .radius(fisheyeRadius)         //size in percent
    .distortion(fisheyeStrength);

var fisheyeGrid = d3.fisheye.circular()
    .radius(fisheyeRadius / 100 * innerG.innerW())         //size in percent
    .distortion(fisheyeStrength);

//add group for distortion UI elements
var fisheyeCircle = innerG.group.append("g").append("ellipse")      //use ellipse because circle would be moved by fisheye distortion
    .attr("cx", "50%")
    .attr("cy", "50%")
    .attr("rx", fisheyeRadius + "%")
    .attr("ry", fisheyeRadius * innerG.innerW() / innerG.innerH() + "%")     //recalculate semi-minor axis to yield circular shape
    .attr("stroke","#B0B0B0")
    .attr("stroke-width","0.05em")
    .attr("fill-opacity", 0)
    .attr("clip-path", "url(#clipPenaltyCircle)");

//hide fisheye circle
fisheyeCircle.attr('visibility', "hidden");

//scales for grid distortion
var xScalePixels = d3.fisheye.scale(d3.scale.linear).domain([0, innerG.innerW()]).range([0, innerG.innerW()]);
var yScalePixels = d3.fisheye.scale(d3.scale.linear).domain([innerG.innerH(), 0]).range([innerG.innerH(), 0]);
var xScalePixelsAxis = d3.fisheye.scale(d3.scale.linear).domain([0, innerG.innerW()]).range([0, innerG.innerW()]);
var yScalePixelsAxis = d3.fisheye.scale(d3.scale.linear).domain([innerG.innerH(), 0]).range([innerG.innerH(), 0]);

var xScalePercent = d3.fisheye.scale(d3.scale.linear).range([0, sectionsHoriz.draw * 100]);
var yScalePercent = d3.fisheye.scale(d3.scale.linear).range([sectionsVert.draw * 100, 0]);
//---------------------------------------------------------------------------------------------------------------


function setupDistortion() {
  innerG.group.on("mousemove", function() {
      updateDistortion(this);
  });

  innerG.group.on("touchmove", function() {
    d3.event.stopPropagation();
    d3.event.preventDefault();
    updateDistortion(this);
  });
  
  setupDistortionSwitch();
}


function setupDistortionSwitch() {
  d3.selectAll("#distortion-switch a")
    .on("click", function(){
      var link = d3.select(this);

      enableDistortion(link.attr("data-distortion"));

      d3.event.stopPropagation();
      d3.event.preventDefault();
    });
  enableDistortion("none");
  
  setupFisheyeParamInputs();
}

/*
 * updates the radius for fisheye distortion.
 * @param radius: new radius in %.
 */
function updateFisheyeRadius(newRadius) {
    fisheyeRadius = newRadius;
    
    //update radius
    fisheye.radius(fisheyeRadius);
    fisheyeGrid.radius(fisheyeRadius / 100 * innerG.innerW());
    fisheyeCircle.attr("rx", fisheyeRadius + "%").attr("ry", fisheyeRadius * innerG.innerW() / innerG.innerH() + "%");
}

function updateFisheyeStrength(newStrength) {
    fisheyeStrength = newStrength;
    
    fisheye.distortion(fisheyeStrength);
    fisheyeGrid.distortion(fisheyeStrength);
}

/*
 * enables or disables fisheye distortion.
 */
function distortionFisheye(enable) {
    if (typeof(enable) == "undefined")
        return;

    if (enable) {
        //enable fisheye lens indicator
        fisheyeCircle.attr('visibility', "visible");
    }
    else {
        //hide fisheye lens edge indicator
        fisheyeCircle.attr('visibility', "hidden");
    }
}

/*
 * toggles distortion:
 * none -> fisheye -> cartesian -> none -> ...
 */
function toggleDistortion() {
    //set from none to fisheye
    if (distortionType == "none") {
        distortionType = "fisheye";

        distortionFisheye(true);
    }

    //set from fisheye to cartesian
    else if (distortionType == "fisheye") {
        distortionType = "cartesian";

        distortionFisheye(false);
//        distortionCartesian(true);
    }

    //set from cartesian to none
    else {
        distortionType = "none";
//        distortionCartesian(false);
    }

    //restore view and grid
    zoomed();
}

function toggleGrid() {
    gridEnabled = !gridEnabled;
}

function updateDistortion(container) {
  if (distortionType == "none")
      return;

  //get mouse position in pixels within innerG group
  var mousePosition = d3.mouse(container);
  mousePosition[0] = Math.min(Math.max(parseInt(mousePosition[0]), 0), innerG.innerW());
  mousePosition[1] = Math.min(Math.max(parseInt(mousePosition[1]), 0), innerG.innerH());

  //convert mouse position to percent
  var position = [2];     //mouse position in %
  position[0] = (mousePosition[0] * 100 * sectionsHoriz.draw / innerG.innerW());
  position[1] = (mousePosition[1] * 100 * sectionsVert.draw / innerG.innerH());

  //---------------------------------------------------------------------------------------------------------------
  //cartesian distortion
  if (distortionType == "cartesian") {
      xScalePercent.focus(position[0]);
      yScalePercent.focus(position[1]);

      //data distortion. needs to be done in percent:
      //select all circles in the SVG element
      var circles = innerG.group.selectAll("circle");

      //apply distortion.
      circles.each(function(d) {
          d.x =  xScalePercent(d[currentX]);
          d.y =  yScalePercent(d[currentY]);
      })
          .attr("cx", function(d) { return d.x+ "%"; })
          .attr("cy", function(d) { return d.y+ "%"; });

      xScalePixels.focus(mousePosition[0])
      yScalePixels.focus(mousePosition[1])

      xScalePixelsAxis.focus(mousePosition[0])
      yScalePixelsAxis.focus(mousePosition[1])

      //apply distortion
      innerG.group.selectAll("path")
          .attr("d", function(d) {
              return gridLine(d.map(function(d) {
                  return [
                      xScalePixels(d[0]),
                      yScalePixels(d[1])
                  ];
              } ) );
          });
  }

  //---------------------------------------------------------------------------------------------------------------
  //fisheye distortion
  else if (distortionType == "fisheye") {
      //set fisheye distortion center
      fisheye.focus(position);

      //grid distortion needs to be done with its own fisheye
      fisheyeGrid.focus(mousePosition);      //use pixels instead of % - (path does not support %)

      //draw fisheyeCircle at mouse position
      fisheyeCircle
          .attr("cx", position[0] + "%")
          .attr("cy", position[1] + "%");

      //distort grid
      innerG.group.selectAll("path")
          .attr("d", function(d) {
              return gridLine(d.map(function(d) {
                  var retVal = fisheyeGrid({
                      x: d[0],
                      y: d[1]
          });

                  return [retVal.x, retVal.y];
              } ) );
          });

      //select all circles in the SVG element
      var circles = innerG.group.selectAll("circle");

      //apply distortion.
      //for each circle, pass the data to fisheye for distortion
      //and then apply the updated positions
      circles.each(function(d) {
          //convert coordinates from data domain to svg-%
          var currX =  parseFloat(xScale(d[currentX]));
          var currY =  parseFloat(yScale(d[currentY]));

          d.fisheye = fisheye({
              x: currX,
              y: currY
          });
      })
      .attr("cx", function(d) { return d.fisheye.x + "%"; })    //d.fisheye.x is the updated position
      .attr("cy", function(d) { return d.fisheye.y + "%"; })
  }

  //update axes.
  XDimensionsG.group.call(xAxis.axis);
  YDimensionsG.group.call(yAxis.axis);
}

function enableDistortion(distortion) {
  switch(distortion) {
  case "none":
    if(distortionType == "cartesian") {
        //reset original scales
        xAxis.axis.scale(xAxisScale);
        yAxis.axis.scale(yAxisScale);

        zoom.x(xAxis.axis.scale());
        zoom.y(yAxis.axis.scale());
        resetView();
    }
    fisheyeEnabled = false;
    cartesianEnabled = false;
    distortionType = "none";
    break;
  case "fisheye":
    if(distortionType == "cartesian") {
        //reset original scales
        xAxis.axis.scale(xAxisScale);
        yAxis.axis.scale(yAxisScale);

        zoom.x(xAxis.axis.scale());
        zoom.y(yAxis.axis.scale());
        resetView();
    }
    fisheyeEnabled = true;
    cartesianEnabled = false;
    distortionType = "fisheye";
    break;
  case "cartesian":
    fisheyeEnabled = false;
    cartesianEnabled = true;
    distortionType = "cartesian";

    //copy original axis scales
    xAxisScale = xAxis.axis.scale().copy();
    yAxisScale = yAxis.axis.scale().copy();

    //use settings of original axis scale
    xScalePixelsAxis.range(xAxis.axis.scale().range()).domain(xAxis.axis.scale().domain());
    yScalePixelsAxis.range(yAxis.axis.scale().range()).domain(yAxis.axis.scale().domain());

    //replace axis scales with
    xAxis.axis.scale(xScalePixelsAxis);
    yAxis.axis.scale(yScalePixelsAxis);
    break;
  }
    
    //"refresh" scales and domains.
    if (typeof dimensions[currentX] !== "undefined" && 
        typeof dimensions[currentY] !== "undefined") {
        xScale.domain(dimensions[currentX].extent);
        xAxis.axis.scale().domain(dimensions[currentX].extent);
        yScale.domain(dimensions[currentY].extent);
        yAxis.axis.scale().domain(dimensions[currentY].extent);        
    }

    XDimensionsG.group.call(xAxis.axis);
    YDimensionsG.group.call(yAxis.axis);
    
    distortionFisheye(fisheyeEnabled);

    updateDistortion(innerG.group[0][0]);

  if (fisheyeEnabled || cartesianEnabled) {
    disableZoom();
  } else {
    enableZoom();
  }

  d3.selectAll("#distortion-switch li.active").attr("class", "");

  var li = d3.select("#distortion-switch a[data-distortion|=" + distortion + "]")[0][0].parentElement;
  d3.select(li).attr("class", "active");

  //restore view and grid
  zoomed();
}

//-----------------------------------------
// fisheye settings

function setupFisheyeParamInputs() {
  d3.select('input#fisheye-radius').on('input', function() {
      updateFisheyeRadius(this.value);
  });
  
  d3.select('input#fisheye-strength').on('input', function() {
      updateFisheyeStrength(this.value);
  });
}